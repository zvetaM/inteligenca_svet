#!/usr/bin/python
# -*- coding: utf-8 -*-
#	Mesh world for intelligence - Python program for the simulation of a world, in which intelligence might emerge.
#	Copyright (C) 2016, 
#	Matevž Markovič, matevz.markovic@guest.arnes.si
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import numpy as np
import random

#Version
ver = 2

#Parameters of the world
dim = 2
number_of_points = 1000													#Number of points in a dimension
program_length = 4														#Program length in each point
max_frequency = 4
max_eps = 0.2

#Initial settings
random.seed(1)
iteration = 1


#Class representation of a point
class Point:
	def __init__(self):
		self.location = np.random.uniform(0,1,(1,dim)).tolist()
		self.program = []
		for i in range(program_length):
			self.program[i] = random.randint(-program_length,program_length)
		self.pointer = 0												#Points to the currently active signal in the program
		self.frequency = random.randint(1,max_frequency)
		self.eps = random.uniform(0,max_eps)

		
#The main loop
while True:
	
	#compute
	#check if intelligence
	
	iteration = iteration + 1
